var voters = [
  { name: "Bob", age: 30, voted: true },
  { name: "Jake", age: 32, voted: true },
  { name: "Kate", age: 25, voted: false },
  { name: "Sam", age: 20, voted: false },
  { name: "Phil", age: 21, voted: true },
  { name: "Ed", age: 55, voted: true },
  { name: "Tami", age: 54, voted: true },
  { name: "Mary", age: 31, voted: false },
  { name: "Becky", age: 43, voted: false },
  { name: "Joey", age: 41, voted: true },
  { name: "Jeff", age: 30, voted: true },
  { name: "Zack", age: 19, voted: false }
];

var youth = voters.filter(function(value) {
  return value.age >= 18 && value.age <= 25;
});

var youthvotes = youth.filter(function(m) {
  return m.voted == true;
});
console.log("Youth:", youth.length);
console.log("YouthVotes:", youthvotes.length);

var mids = voters.filter(function(value) {
  return value.age >= 26 && value.age <= 35;
});

var midvotes = mids.filter(function(n) {
  return n.voted == true;
});

console.log("Mid:", mids.length);
console.log("MidVotes:", mids.length);

var olds = voters.filter(function(value) {
  return value.age >= 36 && value.age <= 55;
});

var oldvotes = olds.filter(function(m) {
  return m.voted == true;
});

console.log("olds:", olds.length);
console.log("oldVotes:", olds.length);

// function voterResults(arr) {
//   // console.log(youngVotes, youth, midVotes, midVotes, oldVotes, olds);
// }
// console.log(voterResults(voters)); // Returned value shown below:

/*
 { 
  youngVotes: 1,
  youth: 4,
  midVotes: 3,
  mids: 4,
  oldVotes: 3,
  olds: 4
 }
 */
// Include how many of the potential voters were in the ages 18-25,
// how many from 26-35, how many from 36-55, and how many of each of those age
//ranges actually voted.
// The resulting object containing this data should have 6 properties.
// See the example output at the bottom.
